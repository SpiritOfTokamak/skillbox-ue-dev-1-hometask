// Copyright Epic Games, Inc. All Rights Reserved.

#include "HomeTask.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HomeTask, "HomeTask" );
