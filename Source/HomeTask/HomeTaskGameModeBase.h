// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeTaskGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMETASK_API AHomeTaskGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
